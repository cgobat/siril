#ifndef SRC_GUI_PHOTOMETRIC_CC_H_
#define SRC_GUI_PHOTOMETRIC_CC_H_

#include <stdio.h>
#include <glib.h>

void initialize_photometric_cc_dialog();
int get_photometry_catalog_from_GUI();

#endif /* SRC_GUI_PHOTOMETRIC_CC_H_ */
